1. To add all file
```bash
git add .
```

2. To commit with msg
```bash
git commit -m "the message"
```

3. To push
```bash
git push origin branch_name
```

4. To pull
```bash
git pull origin branch_name
```

- When update with the master, branch_name = master

- To make a new branch
```bash
git checkout -b "branch_name"
```

- To switch branches
```bash
git checkout "branch_name"
```

- To get status 
```bash
git status
```

add => ready to be stages (commited)
commit => local repo => update
push => remote repo update